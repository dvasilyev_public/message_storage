import random

from packages.config import get_execution_config
from packages.db_client import Message, DataBaseAdapter, drop_db
from packages.db_client import get_engine, get_session, create_db
from packages.logger import get_logger

logger = get_logger(__name__)


def upload_rows(session):
    logger.info("try upload_rows")
    db = DataBaseAdapter(session=session)
    messages = []

    for i in range(10):
        a, b = random.randint(0, 9), [random.randint(0, 9) for _ in range(3)]
        data = {"app_id": i + 1, "request": {"a": a, "b": b}, "response": {"a": a, "b": b}}
        messages.append(Message("sm999", data))

    for i in range(10000):
        a, b = random.randint(0, 9), [random.randint(0, 9) for _ in range(3)]
        data = {"app_id": i + 1, "request": {"a": a, "b": b}, "response": {"a": a, "b": b}}
        messages.append(Message("strategy_service_mock", data))

    db.add_messages(messages)


if __name__ == "__main__":
    config = get_execution_config()
    engine = get_engine(config=config)
    session = get_session(engine=engine)
    drop_db(engine)
    create_db(engine)
    upload_rows(session)
