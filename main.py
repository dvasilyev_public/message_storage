from flask import Flask, request, jsonify

from packages.config import get_execution_config
from packages.db_client import DataBaseAdapter, get_engine, get_session
from packages.logger import get_logger

logger = get_logger(__name__)
app = Flask(__name__)


# okr_client = OkrClient(config=config)  # todo batch
# okr_client.get(message={})


@app.route("/status")
def status():
    return "ok"


@app.route("/messages", methods=["GET"])
def messages():
    # if not request.json:  todo
    #     abort(400)
    logger.debug("run")

    config = get_execution_config()
    engine = get_engine(config=config)
    session = get_session(engine=engine)

    name = request.args.get("name")
    limit = request.args.get("limit")
    offset = request.args.get("offset")

    db = DataBaseAdapter(session=session)
    data, count = db.get_messages(strategy_name=name, limit=limit, offset=offset)

    return jsonify({"result": "ok", "name": name, "limit": limit, "offset": offset, "count": count, "data": data})


if __name__ == "__main__":
    app.run(host="localhost", port=8080)
