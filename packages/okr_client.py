import logging

from requests import Response

from packages.config import ExecutionConfig
from packages.http_client import HttpClient

logger = logging.getLogger(__name__)


class OkrClient:
    def __init__(self, config: ExecutionConfig):
        self._http_client = HttpClient(base_url=config.OKR_ADDRESS)

    def _perform(self, path: str, request: dict) -> Response:
        response = self._http_client.request(method="GET", uri=path, json=request)
        return response

    def get(self, message: dict) -> dict:
        logger.debug("performing request okr")
        response = self._perform(path="/", request=message)
        assert response.status_code == 200, f"Wrong response.status_code: {response.status_code}"
        return response.json()
