import json
from datetime import datetime

from sqlalchemy import Column, Integer, String, create_engine, DateTime
from sqlalchemy.dialects.postgresql.json import JSONB
from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from packages.logger import get_logger

logger = get_logger(__name__)
Base = declarative_base()


class StrategyMessage(Base):
    __tablename__ = 'strategy_message'
    id = Column(Integer, primary_key=True)
    created_ts = Column(DateTime, nullable=False, default=datetime.utcnow)
    strategy_name = Column(String)
    data = Column(JSONB)

    def __init__(self, strategy_name, data):
        self.strategy_name = strategy_name
        self.data = data

    def __repr__(self):
        return f"[{self.id}, {self.created_ts}, {self.strategy_name}, {self.data}]"


class DataBaseAdapter:
    def __init__(self, session):
        self.session = session
        self.db = StrategyMessage

    def add_messages(self, messages):
        logger.debug(f"try to add records: {messages}")
        message = (self.db(i.strategy_name, i.data) for i in messages)
        try:
            self.session.add_all(message)
            self.session.commit()
            logger.debug("records has been add")
        except IntegrityError as e:
            logger.debug(f"exception: {e}\ntry to rollback")
            self.session.rollback()
            logger.debug("rollback has been performed")
            raise

    def get_messages(self, strategy_name, limit, offset):
        logger.debug(f"try to get strategy_name={strategy_name}, limit={limit}, offset={offset}")
        table = self.session.query(self.db).filter(self.db.strategy_name == strategy_name).order_by(self.db.id)
        data = [json.loads(i.data) for i in table.limit(limit).offset(offset)]
        count = table.count()
        return data, count


class Message:
    def __init__(self, strategy_name, data):
        self.strategy_name = strategy_name
        self.data = json.dumps(data, separators=(",", ":"), ensure_ascii=False, sort_keys=True)


def get_engine(config):
    logger.debug("try get engine")
    return create_engine(config.DB_ADDRESS)


def get_session(engine):
    logger.debug("try get session")
    Session = sessionmaker(bind=engine)
    return Session()


def create_db(engine):
    logger.info("try create db")
    Base.metadata.create_all(engine)


def drop_db(engine):
    logger.info("try drop db")
    Base.metadata.drop_all(engine)
