import os
from dataclasses import dataclass
from typing import Optional


@dataclass
class ExecutionConfig:
    LOG_LEVEL: Optional[str]
    DB_ADDRESS: str
    OKR_ADDRESS: str


class ExecutionConfigEnv(ExecutionConfig):
    def __init__(self):
        self.data = os.environ

    def get(self):
        # self.LOG_LEVEL = "info"
        # self.DB_ADDRESS = "postgresql://postgres:postgres@localhost:5432/db"
        # self.OKR_ADDRESS = "http://localhost:80/okr"
        self.LOG_LEVEL = self.data["LOG_LEVEL"]
        self.DB_ADDRESS = self.data["DB_ADDRESS"]
        self.OKR_ADDRESS = self.data["OKR_ADDRESS"]
        return self


def get_execution_config():
    config = ExecutionConfigEnv()
    return config.get()
