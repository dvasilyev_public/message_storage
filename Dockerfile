FROM python:3.9-buster

WORKDIR /opt/app

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY packages packages
COPY *.py ./

CMD ["sh","-c", "python prepare_db.py && gunicorn -w 4 -b 0.0.0.0:8080 wsgi:app"]
